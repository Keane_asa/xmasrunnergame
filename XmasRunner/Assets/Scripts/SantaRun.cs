﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaRun : MonoBehaviour
{
    public Animator animator;

    public void MoveAnims()
    {
        animator.SetBool("Run", true);
    }

    public void DieAnim()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Die", true);
    }
}
