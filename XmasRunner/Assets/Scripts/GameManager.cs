﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    Transform player;
    public Transform levelGen;
    public float score;
    public float presScore;
    [SerializeField]
    LevelGenerator levelGenerator;
    public bool gameActive;
    [SerializeField]
    SantaRun santaAnims;
    [SerializeField]
    TextMeshProUGUI scoreText;
    [SerializeField]
    UIManager uIManager;

    void Update()
    {
        if(!gameActive && !UIManager.paused)
        {
            LevelActive();
        }
        CalculateAndDisplayScore();
    }

    void LevelActive()
    {
        gameActive = true;
        santaAnims.MoveAnims();
    }

    void CalculateAndDisplayScore()
    {
        if(PlayerManager.alive)
        {
            score = Vector2.Distance(player.position, levelGen.position) / 10 + presScore; 
            scoreText.text = $"Points: {score.ToString("F0")}";
        }
    }

    public IEnumerator EndOnDelay()
    {
        yield return new WaitForSeconds(3);
        uIManager.finalScoreString = score.ToString("F0");
        uIManager.EndScreen();
    }
}
