﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatScroller : MonoBehaviour
{
    public float ScrollSpeed = -0.5f;
    private Vector2 _savedOffset;
    private Renderer _renderer;
    float rightEdge;
    float leftEdge;
    Vector3 distanceBetweenEdges;

    void Start ()
    {
        CalculateEdges();
        distanceBetweenEdges = new Vector3(rightEdge - leftEdge,0,0);
    }

    void CalculateEdges()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        rightEdge = transform.position.x + spriteRenderer.bounds.extents.x /3f;
        leftEdge = transform.position.x - spriteRenderer.bounds.extents.x /3f;
    }

    void Update() 
    {
        transform.localPosition += ScrollSpeed * Vector3.right * Time.deltaTime;
        if(PassedEdge())
        {
            MoveRightSpriteToOppositeEdge();
        }
    }

    bool PassedEdge()
    {
        return ScrollSpeed > 0 && transform.position.x > rightEdge || ScrollSpeed  < 0 && transform.position.x < leftEdge;
    }

    void MoveRightSpriteToOppositeEdge()
    {
        if(ScrollSpeed > 0)
        {
            transform.position -= distanceBetweenEdges;
        } else {
            transform.position += distanceBetweenEdges;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawCube(new Vector3(rightEdge,0,0), new Vector3(0.1f, 2f, 10f));    
        Gizmos.DrawCube(new Vector3(leftEdge,0,0), new Vector3(0.1f, 2f, 10f));    
    }
}
