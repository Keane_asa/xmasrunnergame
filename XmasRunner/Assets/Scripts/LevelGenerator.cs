﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] chunkTypes;
    public GameObject currentChunk;
    int currentTiles;
    int maxTiles = 5;
    Vector2 spawnerLocation;
    public int moveAmount;
    public GameObject levelGenerator;
    public List<GameObject> levelChunks = new List<GameObject>();
    public Transform player;

    void Start()
    {
        spawnerLocation = new Vector2(20,0);    
    }
    
    void Update()
    {
        if(currentTiles < maxTiles)
        {
            currentChunk = chunkTypes[Random.Range(0, chunkTypes.Length)];
            GameObject spawnedChunk = Instantiate(currentChunk, spawnerLocation, Quaternion.identity) as GameObject;
            spawnerLocation = new Vector2(spawnerLocation.x + moveAmount, spawnerLocation.y);
            spawnedChunk.transform.SetParent(levelGenerator.transform, false);
            levelChunks.Add(spawnedChunk);
            currentTiles++;
        }

        float dist = Vector3.Distance(player.position, levelChunks[0].transform.position);
        if(dist > 30 && PlayerManager.alive)
        {
            Destroy(levelChunks[0]);
            levelChunks.Remove(levelChunks[0]);
            currentTiles--;
        }
    }
}

