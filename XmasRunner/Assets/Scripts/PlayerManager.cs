﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rb;
    [SerializeField]
    LayerMask ground;
    [SerializeField]
    Transform groundCheck;
    [SerializeField]
    float checkRadius;
    [SerializeField]
    float speed;
    [SerializeField]
    float jumpForce;
    [SerializeField]
    float jumpTime;
    [SerializeField]
    float jumpTImeCounter;
    public bool isGrounded;
    public bool isJumping;
    public bool canJump;
    public static bool alive;
    [SerializeField]
    SantaRun animationController;
    [SerializeField]
    GameObject[] presents;
    [SerializeField]
    GameManager gameManager;
    [SerializeField]
    int scorePerPres;
    [SerializeField]
    AudioClip[] clips;
    [SerializeField]
    AudioSource audioSource, presentAudio;

    void Start()
    {
        alive = true;    
    }

    void Update()
    {
        GroundCheck();
    
        if(alive && !UIManager.paused && canJump)
        {
            if(isGrounded && Input.GetAxis("Jump") > 0)
            {
                if(!isJumping)
                {
                    audioSource.clip = clips[1];
                    audioSource.Play();
                }
                isJumping = true;
                jumpTImeCounter = jumpTime;
                rb.velocity = Vector2.up * jumpForce;
            }

            if(isJumping && Input.GetAxis("Jump") > 0)
            {
                if(jumpTImeCounter > 0)
                {
                    rb.velocity = Vector2.up * jumpForce;
                    jumpTImeCounter -= Time.deltaTime;
                } else {
                    isJumping = false;
                }  
            }

            if(Input.GetAxis("Jump") <= 0)
            {
                isJumping = false;
            }
        } else {
            transform.position = transform.position;
        }
    }

    void GroundCheck()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, ground);
    }
        
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Hazard")
        {
            audioSource.clip = clips[0];
            audioSource.Play();
            alive = false;
            animationController.DieAnim();
            gameManager.StartCoroutine(gameManager.EndOnDelay());
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Hazard" && alive)
        {
            presentAudio.Play();
            Vector2 presLocation = new Vector2(other.transform.position.x, this.transform.position.y);
            int randPres = Random.Range(0, presents.Length);
            GameObject dropApres = Instantiate(presents[randPres], presLocation, Quaternion.identity) as GameObject;
            dropApres.transform.SetParent(gameManager.levelGen.transform, true);
            gameManager.presScore += scorePerPres;
            StartCoroutine(DestroyThisTimed(dropApres));
        }    

        if(other.tag == "KillZone")
        {
            audioSource.clip = clips[0];
            audioSource.Play();
            alive = false;
            animationController.DieAnim();
            gameManager.StartCoroutine(gameManager.EndOnDelay());
        }
    }

    IEnumerator DestroyThisTimed(GameObject toDestroy)
    {
        yield return new WaitForSeconds(10);
        Destroy(toDestroy);
    }

    public void PauseHover(bool isHovering)
    {
        if(isHovering)
        {
            canJump = false;
        } else { 
            canJump = true;
        }
    }
}
