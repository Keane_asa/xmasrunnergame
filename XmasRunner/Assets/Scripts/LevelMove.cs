﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMove : MonoBehaviour
{
    public float speed;
    public GameManager gameManager;
    [SerializeField]
    bool speedAdjusted;
    
    void Update()
    {
        if(gameManager.gameActive)
        {
            if(Screen.width < 1920 && !speedAdjusted)
            {
                speed = speed * 0.75f;
                speedAdjusted = true;
            } 
            if(PlayerManager.alive)
            {
                Vector2 direction = new Vector3(-1,0);
                this.transform.Translate(direction * speed * Time.deltaTime);
            }
        }
    }
}
