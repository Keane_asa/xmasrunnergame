﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject pauseButton;
    public GameObject PausePanel;
    public GameObject instructPanel;
    public GameObject EndPanel;
    [SerializeField]
    Text endText;
    public string finalScoreString;
    public bool gameScreen;
    public bool doYouWantToDisableTransPanel;
    public static bool paused;
    [SerializeField]
    GameObject scoreHolder;
    
    void Start()
    {
       if(gameScreen){
           pauseButton.SetActive(false);
           InstructionEnable(false);
           EndPanel.SetActive(false);
       }  
    }

    private void Update()
    {
        if(paused)
        {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
    }

    public void Pause (){
        paused = true;
        pauseButton.SetActive(false);
        PausePanel.SetActive(true);
        
    }

    public void UnPause (){
        paused = false;
        pauseButton.SetActive(true);
        PausePanel.SetActive(false);
        
    }

    public void InstructionEnable(bool fromPause){
        paused = true;
        instructPanel.SetActive(true);
        
        if(fromPause == true){
        PausePanel.SetActive(false);
        }
    }

    public void CloseInstructions(){
        paused = false;
        instructPanel.SetActive(false);
        
        pauseButton.SetActive(true);
    }

    public void EndScreen (){
        EndPanel.SetActive(true);
        pauseButton.SetActive(false);
        endText.text = $"You scored {finalScoreString} points! \n \n Play again to try to beat your score.";

        scoreHolder.SetActive(false);
    }

}
